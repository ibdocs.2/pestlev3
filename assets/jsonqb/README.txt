In the Psychology and Geography QBs, the topics have either the prefix "2018" (Last exams 2018) or "2019" (First exams 2019), and they have further subdivisions (included in the topic names) that must be differentiated:

Geography (2018): Core, Optional themes, HL extension: Global interactions
Psychology:
	(2018): Core, Options, Qualitative research methodology
	(2019): Core, Options

Same thing for History:
	(2016): Prescribed subjects, Topics, HL options
	(2017): Prescribed subjects, World history topics, HL options
=========================
In the Geography QB, there are some questions that don't include year tag, so they're formatted like this: .1.BP.1
=========================